Source: r-cran-fastdummies
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-fastdummies
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-fastdummies.git
Homepage: https://cran.r-project.org/package=fastDummies
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-data.table,
               r-cran-tibble,
               r-cran-stringr
Testsuite: autopkgtest-pkg-r

Package: r-cran-fastdummies
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: fast creation of dummy columns and rows from categorical variables
 Creates dummy columns from columns that have
 categorical variables (character or factor types). You can also
 specify which columns to make dummies out of, or which columns to
 ignore. Also creates dummy rows from character, factor, and Date
 columns. This package provides a significant speed increase from
 creating dummy variables through model.matrix().
